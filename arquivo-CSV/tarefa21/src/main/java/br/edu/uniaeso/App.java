package br.edu.uniaeso;

//21
//Modifique o programa da Questão 20 para permitir que o usuário insira detalhes de produtos,
// como nome, preço e quantidade, e adicione esses produtos ao arquivo CSV "produtos.csv" usando a biblioteca OpenCSV.

import java.util.Scanner;

public class App {

    public static void main(String[] args) {
        Scanner ler = new Scanner(System.in);

        System.out.println("Informe o nome do produto:");
        String nome = ler.nextLine();

        System.out.println("Informe o preço do produto:");
        double preco = ler.nextDouble();

        System.out.println("Informe a quantidade do produto:");
        int quantidade = ler.nextInt();

        Produto.adicionarProdutoAoCSV(nome, preco, quantidade);

        System.out.println("Produto adicionado com sucesso ao arquivo CSV!");
        ler.close();
    }

}


