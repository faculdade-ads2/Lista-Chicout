package br.edu.uniaeso;

//23
//Adicione a funcionalidade de exclusão de produtos no programa da Questão 20. 
//Permita que o usuário pesquise um produto pelo nome e, em seguida, exclua-o do arquivo CSV 
//"produtos.csv". Certifique-se de atualizar o arquivo CSV após a exclusão.

import java.util.Scanner;

public class App {

    public static void main(String[] args) {
        Scanner ler = new Scanner(System.in);
        Produto produto = new Produto();

        System.out.println("Escolha uma opção:");
        System.out.println("1. Adicionar novo produto");
        System.out.println("2. Atualizar produto existente");
        System.out.println("3. Excluir produto");

        int opcao = ler.nextInt();
        ler.nextLine();
        switch (opcao) {
            case 1:
                produto.adicionarNovoProduto(ler);
                break;
            case 2:
                produto.atualizarProdutoExistente(ler);
                break;
            case 3:
                produto.excluirProduto(ler);
                break;
            default:
                System.out.println("Opção inválida!");
                break;
        }
        
        ler.close();
    }
}


