package br.edu.uniaeso;

//11
//Escreva um programa Java que serializa um objeto da 
//classe Pessoa em um arquivo binário chamado "pessoa.dat".

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;


public class App {
    public static void main(String[] args) {
        Pessoa ps1 = new Pessoa("João", 30);
        Pessoa ps2 = new Pessoa("Maria", 21);
        Pessoa ps3 = new Pessoa("joaquina mariquinha", 7);
        Pessoa ps4 = new Pessoa("joshua", 10);
        
        try (FileOutputStream fileOut = new FileOutputStream("pessoa.dat");
             ObjectOutputStream out = new ObjectOutputStream(fileOut)) {
            out.writeObject(ps1);
            out.writeObject(ps2);
            out.writeObject(ps3);
            out.writeObject(ps4);
            System.out.println("Objeto serializado com sucesso!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}