package br.edu.uniaeso;

//12
//Modifique o programa anterior para deserializar o objeto do arquivo 
//"pessoa.dat" e exibir os detalhes da pessoa no console.

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class App {
    public static void main(String[] args) {

        try (FileInputStream fileIn = new FileInputStream("pessoa.dat");
             ObjectInputStream in = new ObjectInputStream(fileIn)) {
            while (true) {
                try {
                    Pessoa pessoa = (Pessoa) in.readObject();
                    System.out.println("Detalhes da pessoa: " + pessoa);
                } catch (EOFException e) {
                    break;
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
