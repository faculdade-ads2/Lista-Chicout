package br.edu.uniaeso;

//16
//Escreva um programa que leia os primeiros 100 bytes de um arquivo 
//binário chamado "arquivo.bin" e os exiba no console.

import java.io.FileInputStream;
import java.io.IOException;

public class App {
    public static void main(String[] args) {
        int tamanhoLeitura = 100;

        try (FileInputStream fileIn = new FileInputStream("arquivo.bin")) {
            byte[] buffer = new byte[tamanhoLeitura];
            int bytesLidos = fileIn.read(buffer);

            if (bytesLidos != -1) {
                System.out.println("Bytes lidos: ");
                for (int i = 0; i < bytesLidos; i++) {
                    System.out.print((char) buffer[i]);
                }
                System.out.println();
            } else {
                System.out.println("Nenhum byte foi lido do arquivo.");
            }
        } catch (IOException e) {
            System.err.println("Erro ao ler o arquivo: " + e.getMessage());
        }
    }
}

