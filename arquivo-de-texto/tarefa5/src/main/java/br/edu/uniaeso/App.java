package br.edu.uniaeso;

//5
//Crie um programa Java que leia o conteúdo do arquivo 
//"meuarquivo.txt", substitua todas as ocorrências da palavra 
//"Java" pela palavra "Python" e salve o resultado em um novo arquivo chamado "meuarquivo_python.txt".

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class App {
    public static void main(String[] args) {

        String linha;
        
        try (BufferedReader reader = new BufferedReader(new FileReader("meuarquivo.txt"));
             FileWriter writer = new FileWriter("meuarquivo_python.txt")) {
            while ((linha = reader.readLine()) != null) {
                writer.write(linha.replace("Java", "Python") + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
