package br.udu.uniaeso;

//6
//Implemente um programa Java que leia um arquivo CSV chamado 
//"dados.csv" (com valores separados por vírgula) e exiba cada linha no console, dividida em campos.

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class App {
    public static void main(String[] args) {
        
        String linha;

        try (BufferedReader reader = new BufferedReader(new FileReader("dados.csv"))) {
            while ((linha = reader.readLine()) != null) {
                String[] fields = linha.split(",");
                for (String field : fields) {
                    System.out.print(field + " ");
                }
                System.out.println();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
