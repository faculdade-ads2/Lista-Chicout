package br.edu.uniaeso;

//7
////Escreva um programa Java que leia o arquivo 
//"meuarquivo.txt", ordene as linhas em ordem alfabética e salve 
//o resultado em um novo arquivo chamado "meuarquivo_ordenado.txt".

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class App {
    public static void main(String[] args) {

        List<String> lines = new ArrayList<>();
        String linha;

        try (BufferedReader reader = new BufferedReader(new FileReader("meuarquivo.txt"))) {
            
            while ((linha = reader.readLine()) != null) {
                lines.add(linha);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Collections.sort(lines);

        try (FileWriter writer = new FileWriter("meuarquivo_ordenado.txt")) {
            for (String line : lines) {
                writer.write(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


