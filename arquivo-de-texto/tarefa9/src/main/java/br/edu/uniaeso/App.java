package br.edu.uniaeso;

///9
//Crie um programa Java que leia o arquivo 
//"meuarquivo.txt" e conte quantas vezes a palavra "Java" aparece no texto. Exiba o resultado no console.

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class App {
    public static void main(String[] args) {

        int contador = 0;
        String linha;

        try (BufferedReader reader = new BufferedReader(new FileReader("meuarquivo.txt"))) {
            
            while ((linha = reader.readLine()) != null) {
                int index = linha.indexOf("Java");
                while (index != -1) {
                    contador++;
                    index = linha.indexOf("Java", index + 1);
                }
            }
            System.out.println("A palavra 'Java' aparece " + contador + " vezes.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}