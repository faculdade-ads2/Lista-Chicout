package br.edu.uniaeso;

//2
//Crie um programa Java que leia o arquivo 
//"meuarquivo.txt" criado na pergunta anterior e exiba seu conteúdo no console.

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class App {
    public static void main(String[] args) {

        String linha;
        
        try (BufferedReader reader = new BufferedReader(new FileReader("meuarquivo.txt"))) {
            while ((linha = reader.readLine()) != null) {
                System.out.println(linha);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
