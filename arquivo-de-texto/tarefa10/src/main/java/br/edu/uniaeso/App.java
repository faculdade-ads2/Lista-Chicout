package br.edu.uniaeso;

//10
//Escreva um programa Java que crie dois arquivos de texto ("arquivo1.txt" e "arquivo2.txt") com algum conteúdo. 
//Em seguida, crie um terceiro arquivo chamado "arquivo_concatenado.txt"
// que contenha o conteúdo dos dois primeiros arquivos concatenados.

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class App {
    public static void main(String[] args) {

        String arq1 = "arquivo1.txt";
        String arq2 = "arquivo2.txt";
        String arq_concatenado = "arquivo_concatenado.txt";

        try {

            FileWriter writer1 = new FileWriter(arq1);
            FileWriter writer2 = new FileWriter(arq2);
            writer1.write("Conteúdo do arquivo 1\n");
            writer2.write("Conteúdo do arquivo 2\n");
            writer1.close();
            writer2.close();

            List<String> linesFromFile1 = Files.readAllLines(Paths.get(arq1));
            List<String> linesFromFile2 = Files.readAllLines(Paths.get(arq2));
            FileWriter writer3 = new FileWriter(arq_concatenado);

            for (String line : linesFromFile1) {
                writer3.write(line + "\n");
            }
            for (String line : linesFromFile2) {
                writer3.write(line + "\n");
            }

            writer3.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
