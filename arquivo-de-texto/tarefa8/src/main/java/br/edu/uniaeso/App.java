package br.edu.uniaeso;

//8
//Implemente um programa Java que leia o arquivo 
//"meuarquivo.txt" e remova todas as linhas que contêm a palavra 
//"excluir". Salve o resultado em um novo arquivo chamado "meuarquivo_sem_excluir.txt".


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class App{
    public static void main(String[] args) {

        String inputFile = "meuarquivo.txt";
        String outputFile = "meuarquivo_sem_excluir.txt";
        String linha;

        try (BufferedReader reader = new BufferedReader(new FileReader(inputFile));
             FileWriter writer = new FileWriter(outputFile)) {

            while ((linha = reader.readLine()) != null) {
                if (!linha.contains("excluir")) {
                    writer.write(linha + "\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}