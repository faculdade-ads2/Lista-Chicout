package br.edu.uniaeso;

//1
//Escreva um programa Java que crie um arquivo de texto chamado 
//"meuarquivo.txt" e escreva a mensagem "Olá, mundo!" nele.

import java.io.FileWriter;
import java.io.IOException;

public class App {
    public static void main(String[] args) {
        try {
            FileWriter arq= new FileWriter("meuarquivo.txt");
            arq.write("Olá, mundo!");
            arq.close();
            System.out.println("Arquivo encontrado!");
        } catch (IOException e) {
            System.out.println("Arquivo não encontrado");
            e.printStackTrace();
        }
    }
}

