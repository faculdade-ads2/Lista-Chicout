package br.edu.uniaeso;

//4
//Escreva um programa Java que conte quantas linhas existem no arquivo 
//"meuarquivo.txt" e exiba esse número no console.

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class App {
    public static void main(String[] args) {

        int contador_linha = 0;
        
        try (BufferedReader reader = new BufferedReader(new FileReader("meuarquivo.txt"))) {
            while (reader.readLine() != null) {
                contador_linha++;
            }
            System.out.println("Número de linhas: " + contador_linha);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}