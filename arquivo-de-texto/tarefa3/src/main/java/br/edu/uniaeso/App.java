package br.edu.uniaeso;

//3
//Modifique o programa anterior para adicionar a mensagem 
//"Isso é uma adição!" ao final do arquivo "meuarquivo.txt" sem apagar o conteúdo existente.

import java.io.FileWriter;
import java.io.IOException;

public class App {

    public static void main(String[] args) {

        try (FileWriter writer = new FileWriter("meuarquivo.txt", true)) {
            writer.write("\n Isso é uma adição!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}